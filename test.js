let excelParser = require('./index');
let fs = require('fs');
let path = require('path');
let data = fs.readFileSync('./test.xlsx');
let p = new excelParser([
    {name: '序号', type: 'integer', required: true, example: '1'},
    {name: '姓名', dataName: 'name', type: 'string', min: 1, max: 32, example: '张三'},
    {name: '电话', dataName: 'phone', type: 'string', example: '18977700000'},
    {name: '年龄', dataName: 'age', type: 'integer', example: '20'},
    {name: '性别', dataName: 'gender', type: 'enum', value: ['男', '女'], example: '男', required: true},
]);
// p.getExampleFile().then((result)=>{
//     fs.writeFileSync('example.xlsx',result);
// });
let values = {extractDate: new Date(),
    people: [
        {name: "John Smith", age: 20},
        {name: "Bob Johnson", age: 22}
    ],
    text: 'afs'
};
let file = fs.readFileSync(path.join(__dirname, 'formatted.xlsx'));
p.manualExport(file,values).then((b)=>{
    fs.writeFileSync('output.xlsx', b, 'binary');
});
//example file
// p.loadFile(data).then(() => {
//     return p.getExampleFile()
// }).then((result) => {
//     console.log(result);
// }).catch((err) => {
//     console.error(err);
// });